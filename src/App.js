import React from "react";
import { BrowserRouter, Redirect, Route, Switch, useParams } from "react-router-dom";
import { StoreProvider } from "./util/useStore";
import './App.less';
import Landing from "./pages/Landing";
import RegisterUser from "./pages/RegisterUser";
import LoginUser from "./pages/LoginUser";
import ForgotPassword from  './pages/ForgotPassword'
import ResetPassword from './pages/ResetPassword'
import VerifyToken from './pages/VerifyToken'
import Profile from "./pages/Profile";

const App = () => {
  return (
    <div style={{
      height: '100vh',
      width: '100vw',
      overflowY: 'hidden',
      overflowX: 'hidden'
    }}>
      <StoreProvider>
        <BrowserRouter>
          <Switch>
            <Route exact={true} path={'/'} >
              <Redirect to={'/index'}/>
            </Route>
            <Route exact={true} path={'/index'} component={Landing}/>
            <Route exact={true} path={'/register'} component={RegisterUser}/>
            <Route exact={true} path={'/login'} component={LoginUser}/>
            
            <Route exact={true} path={'/forgot-password'} component={ForgotPassword}/>
            <Route exact={true} path={'/reset-password'} component={ResetPassword}/>
            <Route exact={true} path={'/active/:token'} component={VerifyToken}/>
            <Route exact={true} path={'/profile'} component={Profile}/>
            {/* <Route exact={true} path={'/500'} component={ServerError}/>
            <Route exact={true} path={'/404'} component={NotFoundError}/>
            <Route exact={true} path={'/403'} component={ForbiddenError}/> */}
            <Route>
              <Redirect to={'/404'} />
            </Route>
          </Switch>
        </BrowserRouter>
      </StoreProvider>
    </div>
  )
}

export default App;
