import { makeAutoObservable, configure } from 'mobx'
import { Http } from '../util/http';
import { Auth } from './auth';
import User from './user';

export class AppState{
  http = new Http(this.token);
  user = new User(this);
  auth = new Auth(this);
  constructor(context){
    makeAutoObservable(this)
    configure({
      enforceActions: 'never'
    })
  }
  setToken(token) {
    this.token = token;
    this.http.token = token;
    localStorage.setItem('id_token', token);
  }
  setEmail(email){
    this.email = email;
    localStorage.setItem('email', email)
  }
  setId(id){
    this.id = id;
    localStorage.setItem('id', id)
  }
}

export const storeInstance = new AppState()