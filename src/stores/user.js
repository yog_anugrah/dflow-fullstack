import { message } from 'antd';
import _ from 'lodash';
import { makeAutoObservable } from 'mobx';
import { Redirect } from 'react-router-dom';

export default class User {
  islogin = false;
  emailLabel = ''
  passLabel = ''
  username = ''
  constructor(context){
    makeAutoObservable(this)
    this.context = context;
    this.http = context.http;
  }

  registerUser(value){
    let data = value
    this.context.auth.register(data).then(res => {
      message.success("Success! Please Check Your Email For Activation Link")
      return res
    })
    .catch(err => {
      message.error("There is problem during registration")
      throw err
    })
  }

  loginUser(value){
    let data = value;
    this.context.auth.login(data).then(res => {
      this.islogin = true
      return res
    })
    .catch(err => {
      this.islogin = false
      throw err
    })
  }

  isLoginCheck(){
    const email = localStorage.getItem('email')
    let data = {
      email : email,
      token : localStorage.getItem('id_token')
    }
    if(!this.islogin){
      this.http.post(`/is-login`, data).then(res => {
        this.islogin = res.is_login
        return res
      }).catch(err => {
        throw err
      })
    }
  }

  verifyToken(value){
    this.http.post(`/activate/${value}`).then(res => {
      return res
    }).catch(err => {throw err})
  }

  forgotPassword(value){
    const {email} = value;
    
    this.context.setEmail(email)
    this.http.post('/forgot-password', {email : email}).then(res => {
      message.success("Success! Please Check Your Email For Reset Password Link")
      return res
    })
    .catch(err => {
      message.error("There is problem during registration, Please Try again")
      throw err
    })
  }

  resetPassword(value){
    // localStorage.removeItem('id')
    const data = {
      email : localStorage.getItem('email'),
      new_password : value.password
    }
    this.http.post('/reset-password', data).then(res => {
      message.success("Success Reseting Password, Try Login Again")
      return(
        <Redirect to={'/index'}/>
      )
    }).catch(err => {
      throw err
    })
  }

  userProfile(){
    const data = {
      email : localStorage.getItem('email'),
      token : localStorage.getItem('id_token')
    }
    this.http.post('/profile', data).then(res => {
      console.log(res, "Hasil User")
      this.emailLabel = res.email
      this.username = res._id
    })
  }
}