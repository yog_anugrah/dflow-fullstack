import { makeAutoObservable } from 'mobx'

export class Auth {

  constructor(context){
    makeAutoObservable(this)
    this.context = context;
    this.http = context.http;
  }


  login(data) {
    // this.context.setToken('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJkaWdpc3VpdGVzIiwiZXhwIjoxNDg1ODcyMTYzLCJzdWIiOiJjcmVkZW50aWFsIiwidXNlciI6Imhhc3RhLnJhZ2lsQGdtYWlsLmNvbSIsInJvbGVzIjpbIlRyYXZlbCBBZ2VudCJdfQ.u3MEelOz3FWyP78QLTvTh4n-ueH1G4R0GWcXrjXUPMY');
    return this.http.post("/login", data)
      .then(res => {
        this.context.setToken(res.accessToken);
        this.context.setEmail(res.email)
      })
      .catch(err => {
        throw err;
      })
  }

  logout() {
    this.context.setToken("");
    this.context.setEmail("");
    localStorage.removeItem('id_token');
    localStorage.removeItem('email');
  }

  register(data) {
    return this.http.post("/register", data)
      .then(res => {
        return res;
      })
      .catch(err => {
        throw err;
      })
  }
}