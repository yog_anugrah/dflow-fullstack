import React, {useState} from 'react';
import {Col, Layout, Row, Space, Typography} from 'antd';
import "./ForgotPassword.less"
import ForgotPasswordForm from "./ForgotPassword/ForgotPasswordForm";


const ForgotPassword = () => {
  return (
    <div className='forgot-class'>
      <Layout.Content style={{ display:"flex", alignItems:'center', justifyContent : "center", height : "100%", width: '100%'}}>
        <div className="forgot-container">
          <Row style={{height : '100%'}} gutter={8}>
            <Col span={12}>
              <div className='forgot-form'>
                <Space size={'middle'} direction={'vertical'}>
                  <Typography.Title style={{color : '#FAF0FF'}}>Forgot Password</Typography.Title>
                  <ForgotPasswordForm/>
                </Space>
              </div>
            </Col>
            <Col span={12}>
              <div className='forgot-img'>

              </div>
            </Col>
          </Row>
        </div>
      </Layout.Content>
    </div>
  )
}
export default ForgotPassword