import React, {useState} from "react";
import { Layout, Space, Typography } from "antd";
import "./RegisterUser.less";
import RegistrationForm from "./RegisterUser/RegistrationForm"
const RegisterUser = () => {
  return (
    <div className='register-class'>
      <Layout.Content style={{ display:"flex", alignItems:'center', justifyContent : "center", height : "100%", width: '100%'}}>
        <div className='register-container'>
          <Space direction="vertical">
            <Typography.Title style={{textAlign : "center"}}>Join Us!</Typography.Title>
            <RegistrationForm/>
          </Space>
        </div>
      </Layout.Content>
    </div>
  )
}
export default RegisterUser
