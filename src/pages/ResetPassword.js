import React, { useState } from "react";
import {Layout, Row, Col, Space, Typography} from 'antd'
import "./ResetPassword.less"
import ResetPasswordForm from "./ResetPassword/ResetPasswordForm";

const ResetPassword = () => {
  return (
    <div className='reset-class'>
      <Layout.Content style={{ display:"flex", alignItems:'center', justifyContent : "center", height : "100%", width: '100%'}}>
        <div className='reset-container'>
          <Row style={{height : '100%'}} gutter={8}>
            <Col span={12}>
              <div className='reset-form'>
                <Space direction = 'vertical' size='middle'>
                  <Typography.Title style={{color : '#FAF0FF'}}>Reset Password</Typography.Title>
                  <ResetPasswordForm />
                </Space>
              </div>
            </Col>
            <Col span={12}>
              <div className='reset-img'>

              </div>
            </Col>
          </Row>
        </div>
      </Layout.Content>
    </div>
  )
}

export default ResetPassword