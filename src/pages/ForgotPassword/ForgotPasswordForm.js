import React from 'react';
import { Form, Input, Button, Checkbox, Typography } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router';
import { useStore } from '../../util/useStore';
import { observer } from 'mobx-react';

const ForgotPasswordForm = observer(() => {
  const history = useHistory();
  const store = useStore();
  const userStore = store.user;
  const toLoginPage = () => {
    history.push('/login')
  }
  const onFinish = (values) => {
    console.log('Received values of form: ', values);
    userStore.forgotPassword(values)
  };

  return (
    <Form
      name="normal_login"
      className="login-form"
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
    >
      <Form.Item
        name="email"
        rules={[
          {
            required: true,
            message: 'Please input your Username!',
          },
        ]}
      >
        <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Email" />
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" className="login-form-button">
          Reset Password
        </Button>
        <Typography.Text style={{color : "#FAF0FF"}}> Or </Typography.Text>
        <Typography.Link style={{color : "#E8D3F2"}} onClick={() => toLoginPage()}>Nevermind, I Remember</Typography.Link>
      </Form.Item>
    </Form>
  );
});

export default ForgotPasswordForm;