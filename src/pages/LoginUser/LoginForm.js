import React, {useEffect} from 'react';
import { Form, Input, Button, Checkbox, Typography } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router';
import { useStore } from '../../util/useStore';
import { observer } from 'mobx-react';

const LoginForm = observer(() => {
  const history = useHistory();
  const store = useStore();
  const userStore = store.user;
  const toForgotPage = () => {
    history.push('/forgot-password')
  }
  const toRegisterPage = () => {
    history.push('/register')
  }
  const onFinish = (values) => {
    console.log('Received values of form: ', values);
    userStore.loginUser(values)
    toProfile()
  };

  const toProfile = () => {
    history.push('/profile')
  }

  useEffect(() => {
    // userStore.isLoginCheck()
  })

  return (
    <Form
      name="normal_login"
      className="login-form"
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
    >
      <Form.Item
        name="email"
        rules={[
          {
            required: true,
            message: 'Please input your Username!',
          },
        ]}
      >
        <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Email" />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[
          {
            required: true,
            message: 'Please input your Password!',
          },
        ]}
      >
        <Input
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="Password"
        />
      </Form.Item>
      <Form.Item>
        <Form.Item name="remember" valuePropName="checked" noStyle>
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <Typography.Link onClick={() => toForgotPage()} className="login-form-forgot" >
          Forgot password
        </Typography.Link>
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" className="login-form-button">
          Log in
        </Button>
        Or <Typography.Link onClick={() => toRegisterPage()} >register now!</Typography.Link>
      </Form.Item>
    </Form>
  );
});

export default LoginForm;