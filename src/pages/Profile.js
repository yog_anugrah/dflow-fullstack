import React, {useEffect, useState} from 'react';
import { Button, Layout, Space, Typography } from 'antd';
import "./Profile.less";
import { useStore } from '../util/useStore';
import { toJS } from 'mobx';
import { useHistory } from 'react-router';
import { observer } from 'mobx-react';

const Profile =  observer(() => {

  const history = useHistory()
  const store = useStore();
  const userStore = store.user;
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('')

  useEffect(() => {
    userStore.userProfile()
  })

  const toHomepage = () => {
    history.push("/")
  }

  useEffect(() => {
    setUsername(toJS(userStore.username))
    setEmail(toJS(userStore.emailLabel))
  }, [userStore.emailLabel, userStore.username])
  return (
    <div className='profile-class'>
      <Layout.Content style={{ display:"flex", alignItems:'center', justifyContent : "center", height : "100%", width: '100%'}}>
        <div className='profile-container'>
         <Space size={'middle'} direction={'vertical'}>
            <Typography.Title>Hi {username}</Typography.Title>
            <Typography.Text italic>Your email is :{email}</Typography.Text>
            <Button type={'primary'} onClick={() => {
              store.auth.logout()
              toHomepage()
            }}>Logout</Button>
         </Space>
        </div>
      </Layout.Content>
    </div>
  )
});
export default Profile;