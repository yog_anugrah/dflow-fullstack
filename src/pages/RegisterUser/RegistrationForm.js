import React, { useState } from 'react';
import { Form, Input, Button, Typography} from 'antd';
import { useHistory } from 'react-router';
import { useStore } from '../../util/useStore';
import { observer } from 'mobx-react';
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};

const RegistrationForm = observer(() => {
  const [form] = Form.useForm();

  const history = useHistory();
  const store = useStore();
  const userStore = store.user;
  const toLoginPage = () => {
    history.push('/login')
  }

  const onFinish = (values) => {
    console.log('Received values of form: ', values);
    userStore.registerUser(values)
  };
  return (
    <Form
      {...formItemLayout}
      form={form}
      name="register"
      onFinish={onFinish}
      scrollToFirstError
    >
      <Form.Item
        name="email"
        label="E-mail"
        rules={[
          {
            type: 'email',
            message: 'The input is not valid E-mail!',
          },
          {
            required: true,
            message: 'Please input your E-mail!',
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        name="password"
        label="Password"
        rules={[
          {
            required: true,
            message: 'Please input your password!',
          },
        ]}
        hasFeedback
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        name="confirm"
        label="Confirm"
        dependencies={['password']}
        hasFeedback
        rules={[
          {
            required: true,
            message: 'Please confirm your password!',
          },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue('password') === value) {
                return Promise.resolve();
              }

              return Promise.reject(new Error('The two passwords that you entered do not match!'));
            },
          }),
        ]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Register
        </Button>
        <Typography.Text style={{color : "#7B2CBF"}}> Or </Typography.Text>
        <Typography.Link style={{color : "#3E0F73"}} onClick={() => toLoginPage()}>Login</Typography.Link>
      </Form.Item>
    </Form>
  );
});
export default RegistrationForm