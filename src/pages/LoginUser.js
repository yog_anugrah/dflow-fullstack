import React, {useState} from 'react';
import { Layout, Space, Typography } from 'antd';
import "./LoginUser.less";
import LoginForm from './LoginUser/LoginForm'

const LoginUser = () => {
  return (
    <div className='login-class'>
      <Layout.Content style={{ display:"flex", alignItems:'center', justifyContent : "center", height : "100%", width: '100%'}}>
        <div className='login-container'>
         <Space size={'middle'} direction={'vertical'}>
            <Typography.Title style={{textAlign : 'center'}}>Login</Typography.Title>
            <LoginForm/>
         </Space>
        </div>
      </Layout.Content>
    </div>
  )
};
export default LoginUser;