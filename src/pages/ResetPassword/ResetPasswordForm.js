import React, { useState } from 'react';
import { Form, Input, Button} from 'antd';
import { useHistory } from 'react-router-dom';
import { useStore } from '../../util/useStore';
import { observer } from 'mobx-react';

const ResetPasswordForm = observer(() => {
  const [form] = Form.useForm();

  const history = useHistory();
  const store = useStore();
  const userStore = store.user;

  const onFinish = (values) => {
    console.log('reset form: ', values);
    userStore.resetPassword(values)
  };
  return (
    <Form
      form={form}
      name="register"
      onFinish={onFinish}
      scrollToFirstError
    >

      <Form.Item
        name="password"
        rules={[
          {
            required: true,
            message: 'Please input your password!',
          },
        ]}
        hasFeedback
      >
        <Input.Password placeholder={'New Password'}/>
      </Form.Item>

      <Form.Item
        name="confirm"
        dependencies={['password']}
        hasFeedback
        rules={[
          {
            required: true,
            message: 'Please confirm your password!',
          },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue('password') === value) {
                return Promise.resolve();
              }

              return Promise.reject(new Error('The two passwords that you entered do not match!'));
            },
          }),
        ]}
      >
        <Input.Password placeholder={'Confirm Password'} />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Reset
        </Button>
      </Form.Item>
    </Form>
  );
});
export default ResetPasswordForm