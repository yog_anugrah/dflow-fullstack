import React, {useState} from "react";
import { Button, Carousel, Col, Layout, Row, Space, Typography } from "antd";
import "./Landing.less";
import { Redirect, useHistory } from "react-router-dom";


const Landing = () => {
  const history = useHistory();
  const toHomepage = () => {
    return(
      history.push("/")
    )
  }
  const toRegister = () => {
    return (
      history.push('/register')
    )
  }
  const toLogin = () => {
    return (
      history.push('/login')
    )
  }
  return(
    <div className="landing-class">
      <Layout style={{minHeight : '100%', minWidth : "100%", width : "100%", height : "100%"}}>
        <Layout.Header style={{position: 'fixed', width : "100%"}}>
          {/* <HeaderComponent/> */}
          <div style={{textAlign : 'center', margin : 'auto', width : '100%%'}}> 
            <Row>
              <Col span={2}>
                <image className="home-button" onClick={() => toHomepage()} />
              </Col>
              <Col span={18}/>
              <Col span={4}>
                <Space align={"center"} size={"large"} direction={"horizontal"}>
                  <Typography.Link onClick={() => toLogin()}>Login </Typography.Link>
                  <Button onClick={() => toRegister()} type={"primary"} >Register</Button>
                </Space>
              </Col>
            </Row>
          </div>
        </Layout.Header>
        <Layout.Content style={{marginTop: 64, width : '100%', height:"100%"}}>
          {/* <ContentComponent/> */}
          <div style={{textAlign : 'center', margin : 'auto', width : '100%', height:"100%"}}>
            <Carousel autoplay style={{width : '100%', height:"95vh"}}>
              <div className="bg-image1">
                <div className="landing-container">
                  <Typography>
                    <Typography.Title>
                      Why You Should Start Running
                    </Typography.Title>
                    <Typography.Text italic={true}>
                      Running is amazing for your body, brain, and soul.
                    </Typography.Text>
                  </Typography>
                </div>
              </div>
              <div className="bg-image2">
                <div className="landing-container">
                  <Typography>
                    <Typography.Title>
                      You Should Run With Friends
                    </Typography.Title>
                    <Typography.Text italic={true}>
                    From lending support to eating with you, running friends are pretty cool and that means running friends are the absolute best.
                    </Typography.Text>
                  </Typography>
                </div>
              </div>
              <div className="bg-image3">
                <div className="landing-container">
                  <Space align="center" direction="vertical">
                    <Typography>
                      <Typography.Title>
                        Join Us!
                      </Typography.Title>
                    </Typography>
                    <Typography.Link onClick={() => toLogin()}>Login </Typography.Link>
                    <Button onClick={() => toRegister()} type={"primary"} >Register</Button>
                  </Space>
                </div>
              </div>
            </Carousel>
          </div>
        </Layout.Content>
        {/* <Layout.Footer>
          <div style={{textAlign : 'center', margin : 'auto', width : '50%'}}> FOOTER </div>
          <FooterComponent/>
        </Layout.Footer> */}
      </Layout>
    </div>
  )
}
export default Landing