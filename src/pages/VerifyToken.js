import React, {useState, useEffect} from "react";
import { Layout, Space, Typography } from "antd";
import "./VerifyToken.less";
import { useHistory, useParams } from "react-router-dom";
import { useStore } from "../util/useStore";
import { observer } from "mobx-react";
const VerifyToken = observer(() => {
  const history = useHistory();
  const { token } = useParams();
  const store = useStore();
  const userStore = store.user;
  useEffect(() => {
    // effect
    userStore.verifyToken(token)
  })
  const toLogin = () => {
    return (
      history.push('/login')
    )
  }
  return (
    <div className='verify-class'>
      <Layout.Content style={{ display:"flex", alignItems:'center', justifyContent : "center", height : "100%", width: '100%'}}>
        <div className='verify-container'>
          <Space direction="vertical">
            <Typography.Title style={{textAlign : "center"}}>Activated!</Typography.Title>
            <Typography.Text italic={true}>Your Account Already Activated, Click <Typography.Link onClick={() => toLogin()}>Here</Typography.Link></Typography.Text>
          </Space>
        </div>
      </Layout.Content>
    </div>
  )
})
export default VerifyToken
