import fetch from "isomorphic-fetch";
import {appConfig} from "../config/appConfig";

export class Http {
  token = '';
  baseUrl = '';
  email = ''
  id = ''

  constructor(token, baseUrl = appConfig.apiUrl, disableAuth=false) {
    if (token) {
      this.token = token;
    }
    this.disableAuth = disableAuth;
    this.baseUrl = baseUrl;
  }
  checkStatus(result) {
    if (!result.ok) {
      return result.json().then(data => {
        return Promise.reject(data);
      });
    }
    return result;
  }

  get(url) {
    let headers = new Headers();
    if(!this.disableAuth) {
      headers.append("Authorization", "Bearer " + this.token);
    }
    return fetch(this.baseUrl + url, {
      headers
    }).then(this.checkStatus).then(response => {
      if(response.status === 204) {
        return '';
      } else {
        return response.json();
      }
    }).catch(err => {
      throw err;
    })
  }

  post(url, data, options) {
    let headers = new Headers();
    if(!this.disableAuth) {
      headers.append("Authorization", "Bearer " + localStorage.getItem('id_token'));
    }
    headers.append("Content-Type", "application/json");

    return fetch(this.baseUrl + url, Object.assign({
      method: 'POST',
      headers,
      body: JSON.stringify(data)
    }, options)).then(this.checkStatus).then(response => response.json()).catch(err => {
      throw err;
    })
  }

}