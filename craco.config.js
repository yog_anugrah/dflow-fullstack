const CracoLessPlugin = require('craco-less');
module.exports = {
  plugins: [
      {
          plugin: require("craco-antd"),
          options: {
              customizeTheme: {
                    '@primary-color': '#7B2CBF',
                    "@white": "#FAF0FF",
                    "@black": "#110326",
                    "@primary-1": "#FAF0FF",
                    "@primary-2": "#E8D3F2",
                    "@primary-3": "#CDA3E6",
                    "@primary-4": "#B277D9",
                    "@primary-5": "#9650CC",
                    "@primary-6": "#7B2CBF",
                    "@primary-7": "#5A1C99",
                    "@primary-8": "#3E0F73",
                    "@primary-9": "#25064D",
                    "@primary-10": "#110326",
                    "@layout-header-background" : '@primary-10',
                    "@border-radius-base": '6px',
                    "@component-background" : '#11032600',
                    "@btn-default-color": "@primary-5",
                    "@btn-default-border" : '#11032600',
                    "@input-bg" : "@primary-1",
                    "@label-required-color": "@primary-8",
                    "@message-notice-content-bg" : "@primary-4"
              },
              lessLoaderOptions: {
                  
              }
          }
      }
  ]
};
